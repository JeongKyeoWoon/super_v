var work = [
    /*기타*/
    { "cate":"m01", "dep01":"회원", "dep02":"아이디 찾기", "url":"./dist/member/find_id.html", "date":"2022-11-21" },
    { "cate":"m01", "dep01":"회원", "dep02":"아이디 찾기-결과", "url":"./dist/member/find_id_complete.html", "date":"2022-11-21" },
    { "cate":"m01", "dep01":"회원", "dep02":"아이디 찾기-에러표시", "url":"./dist/member/find_id_error.html", "date":"2022-11-21" },
    { "cate":"m01", "dep01":"회원", "dep02":"비밀번호 찾기", "url":"./dist/member/find_password.html", "date":"2022-11-21" },
    { "cate":"m01", "dep01":"회원", "dep02":"비밀번호 찾기-결과", "url":"./dist/member/find_password_complete.html", "date":"2022-11-21" },
    { "cate":"m01", "dep01":"회원", "dep02":"비밀번호 찾기-에러표시", "url":"./dist/member/find_password_error.html", "date":"2022-11-21" },
    { "cate":"m01", "dep01":"회원", "dep02":"로그인", "url":"./dist/member/login.html", "date":"2022-11-21" },
    { "cate":"m01", "dep01":"회원", "dep02":"로그인-에러표시", "url":"./dist/member/login_error.html", "date":"2022-11-21" },


	{ "cate":"m02", "dep01":"결제", "dep02":"주문 / 결제 - 입력", "url":"dist/payment/payment.html", "date":"2022-11-21" },
    { "cate":"m02", "dep01":"결제", "dep02":"주문 / 결제 - 완료", "url":"dist/payment/order-result.html", "date":"2022-11-21" },
    { "cate":"m02", "dep01":"결제", "dep02":"[팝업]구매 상담 신청", "url":"dist/payment/pop-buy-req.html", "date":"2022-11-21" },

    //{ "cate":"m03", "dep01":"마이페이지", "dep02":"내 정보", "url":"dist/mypage/my-info.html", "date":"2022-11-18" },
    //{ "cate":"m03", "dep01":"마이페이지", "dep02":"결제 정보", "url":"dist/mypage/payment-info.html", "date":"2022-11-18" },
    //{ "cate":"m03", "dep01":"마이페이지", "dep02":"회원 탈퇴 - 사유입력", "url":"dist/mypage/out-form.html", "date":"2022-11-18" },
    //{ "cate":"m03", "dep01":"마이페이지", "dep02":"회원 탈퇴 - 완료", "url":"dist/mypage/out-complete.html", "date":"2022-11-18" },
    //{ "cate":"m03", "dep01":"마이페이지", "dep02":"[팝업]필수입력 사항 미입력01", "url":"dist/mypage/pop-msg01.html", "date":"2022-11-18" },
    //{ "cate":"m03", "dep01":"마이페이지", "dep02":"[팝업]필수입력 사항 미입력01", "url":"dist/mypage/pop-msg02.html", "date":"2022-11-18" },

    { "cate":"m04", "dep01":"제품", "dep02":"소개", "url":"dist/product/product.html", "date":"2022-11-21" },

    { "cate":"m05", "dep01":"이벤트", "dep02":"이벤트", "url":"dist/event/event.html", "date":"2022-11-21" },

    //{ "cate":"m04", "dep01":"기타", "dep02":"앱 설치 현황", "url":"html/popup/pop-app-status.html", "date":"2022-11-21" },


];

 
$(function(){   
	listTable(".siteNavi li", ".siteNavi li .num");
}); 
 
function listTable(cls, num){   
	var tr;
	for(i=0; i<work.length; i++){
		tr += "<tr class="+work[i].cate+">";
		tr += "<td>"+work[i].dep01+"</td>";
		tr += "<td>"+work[i].dep02+"</td>";
		tr += "<td><a href='./"+work[i].url+"' target='_blank'>"+work[i].url+"</a></td>";
		tr += "<td class='ac'>"+work[i].date+"</td>";
		tr += "</tr>"; 
	}  
	$("table tbody").append(tr);  
	
	$(num).each(function(z){
		if(z===0){
			$(num).eq(z).text("("+work.length+"p)");
		}else{
			$(num).eq(z).text("("+$('.m0'+z).length+"p)");
		} 
	}); 
	$("body").on("click",cls, function(){
		$(cls).removeClass("on"); 
		$(this).addClass("on");
		$("table tbody tr").hide();
		if($(this).index() === 0){
			$("table tbody tr").show();
		}else{
			$(".m0"+$(this).index()).show();
		} 
	});  
}  
